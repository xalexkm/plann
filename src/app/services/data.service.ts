import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Item } from '../interfaces/item.interface';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  private apiUrl: string = 'http://localhost:4000/api';
  constructor(private http: HttpClient) {}

  getItems() {
    return this.http.get<any>(this.apiUrl + '/items');
  }
  createItem(item: any) {
    return this.http.post(this.apiUrl + '/items', item);
  }
  deleteItem(targetId: string) {
    return this.http.delete(this.apiUrl + `/items/${targetId}`);
  }
  modifyItem(targetId: string | undefined, changes: any) {
    return this.http.patch(this.apiUrl + `/items/${targetId}`, changes);
  }
}
