import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Item } from '../interfaces/item.interface';
import { DataService } from '../services/data.service';
import { Store } from '@ngrx/store';
import {
  removeItem,
  modifyItem,
  selectItem,
  loadItems,
  createItem,
} from '../../store/slices/items/items.actions';
import { AppState } from 'src/store';
import { FormControl } from '@angular/forms';
import { Items } from 'src/store/slices/items/items.reducer';
import {
  getItemsFeatureState,
  getItemsList,
} from 'src/store/slices/items/items.selectors';

@Component({
  selector: 'pln-planner',
  templateUrl: './planner.component.html',
  styleUrls: ['./planner.component.scss'],
})
export class PlannerComponent implements OnInit {
  public data$ = this.data.getItems();
  mainInput = new FormControl('');
  items$: Observable<Item[]>;
  selectedItem$: Observable<Item | null>;
  constructor(private data: DataService, private store: Store<AppState>) {
    this.items$ = this.store.select(getItemsList);
    this.selectedItem$ = this.store.select((state) => state.items.selectedItem);
  }

  // private observer = {
  //   next: (x: any) => console.log('Retrieved:' + JSON.stringify(x)),
  //   error: (x: any) => console.log('Error:' + JSON.stringify(x)),
  //   complete: () => console.log('Completed'),
  // };

  modifyItem(pos: number, _id: string | undefined, changes: any) {
    this.store.dispatch(modifyItem({ pos, _id, changes }));
  }

  selectItem(pos: number) {
    this.store.dispatch(selectItem({ pos }));
  }
  createItem(title: string) {
    this.store.dispatch(createItem({ title }));
    this.mainInput.setValue('');
  }

  removeItem(targetId: string = '0') {
    this.store.dispatch(removeItem({ targetId }));
  }

  ngOnInit(): void {
    this.store.dispatch(loadItems());
    // this.data$.subscribe((item) => this.store.dispatch(addItem()));
  }
}
