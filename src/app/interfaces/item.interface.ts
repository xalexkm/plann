export interface Item {
  _id?: string;
  pos: number;
  title: string;
  status: boolean;
}
