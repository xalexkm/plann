import { createSelector } from '@ngrx/store';
import { Item } from 'src/app/interfaces/item.interface';
import { Items } from './slices/items/items.reducer';

export interface AppState {
  items: Items;
}
