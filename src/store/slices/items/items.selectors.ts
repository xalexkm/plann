import { createFeatureSelector, createSelector } from '@ngrx/store';
import { Items } from './items.reducer';

export const getItemsFeatureState = createFeatureSelector<Items>('items');

export const getItemsList = createSelector(
  getItemsFeatureState,
  (state) => state.items
);
export const getItemByPos = (pos: number) =>
  createSelector(getItemsList, (state) => state[pos]);
