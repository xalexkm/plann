import { createAction, props } from '@ngrx/store';
import { Item } from 'src/app/interfaces/item.interface';
import { Items } from './items.reducer';

export const createItem = createAction(
  '[Item Component] Item created',
  props<{ title: string }>()
);
export const removeItem = createAction(
  '[Item Component] Item Removed',
  props<{ targetId: string }>()
);
export const selectItem = createAction(
  '[Item Component] Item Selected',
  props<{ pos: number }>()
);
export const modifyItem = createAction(
  '[Item Component] Item modified',
  props<{ pos: number; _id: string | undefined; changes: any }>()
);
export const loadItems = createAction('[Item Component] Load items');
export const loadItemsSuccess = createAction(
  '[Item API] Items loaded successfully',
  props<{ items: Item[] }>()
);
export const loadItemsError = createAction('[Item API] Loading Items Error');

export const loadItemsAfterPost = createAction(
  '[Item Effect] Loading Items After Post'
);
// export const addItem = createAction('[Item Component] Adding item');
export const createItemSuccess = createAction(
  '[Item API] Item Created Success'
);
export const createItemError = createAction('[Item Api] Failed to create item');

export const deleteItemSuccess = createAction(
  '[Item Api] Item Deleted Success'
);
export const deleteItemError = createAction('[Item Api] Failed to remove item');

export const modifyItemError = createAction('[Item Api] Failed to modify item');
