import { createReducer, createSelector, on } from '@ngrx/store';
import { Item } from 'src/app/interfaces/item.interface';
import { AppState } from '../../index';
import * as ItemsActions from './items.actions';

export interface Items {
  items: Item[];
  selectedItem: Item | null;
}

export const initialState: any = {
  items: [],
  selectedItem: null,
};

const _itemsReducer = createReducer(
  initialState,
  on(ItemsActions.createItem, (store, { title }) => {
    return {
      ...store,
      items: [
        ...store.items,
        {
          pos:
            store.items.length > 0
              ? store.items[store.items.length - 1].pos + 1
              : 0,
          title,
          status: false,
        },
      ],
    };
  }),
  on(ItemsActions.removeItem, (state, { targetId }) => {
    return {
      ...state,
      items: [
        ...state.items.filter((item: { _id: string }) => item._id !== targetId),
      ],
      selectedItem:
        // FIX LATER - make it ignore the change if id doesnt match or is null
        state.selectedItem?._id === targetId ? null : state.selectedItem,
    };
  }),
  on(ItemsActions.selectItem, (state, { pos }) => {
    return {
      ...state,
      selectedItem: state.items[pos],
    };
  }),
  on(ItemsActions.modifyItem, (state, { pos, _id, changes }) => {
    const [target] = state.items.filter((item: any) => item.pos === pos);
    const res = Object.entries(state.items[state.items.indexOf(target)]).map(
      ([keyOld, valueOld]) => {
        if (changes[keyOld] === undefined) {
          return [keyOld, valueOld];
        }
        return [keyOld, changes[keyOld]];
      }
    );
    const obj = Object.fromEntries(res);
    return {
      ...state,
      items: [...state.items.slice(0, pos), obj, ...state.items.slice(pos + 1)],
    };
  }),
  on(ItemsActions.loadItemsSuccess, (state, { items }) => {
    return {
      ...state,
      items,
    };
  })
);

// const modifyItemStatus = (state, { toBeModifiedItem }) => {
//   return {
//     ...state,
//     items: [...state.items],
//   };
// };

export function itemsReducer(state: any, action: any) {
  return _itemsReducer(state, action);
}
