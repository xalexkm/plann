import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap, switchMap, take } from 'rxjs/operators';
import { DataService } from 'src/app/services/data.service';
import * as ItemsActions from './items.actions';
import { getItemByPos, getItemsList } from './items.selectors';

@Injectable()
export class ItemsEffects {
  constructor(
    private actions$: Actions,
    private data: DataService,
    private store: Store<any>
  ) {}
  loadItemsEffect$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ItemsActions.loadItems),
      switchMap(() =>
        this.data.getItems().pipe(
          map((items) => {
            for (let i = 0; i < items.length; i++) {
              items[i].pos = i;
            }
            return ItemsActions.loadItemsSuccess({ items });
          }),
          catchError(() => of(ItemsActions.loadItemsError))
        )
      )
    );
  });
  // reloadItemsAfterChanges$ = createEffect(() => {
  //   return this.actions$.pipe(
  //     ofType(ItemsActions.loadItemsAfterPost),
  //     switchMap(() => {})
  //   );
  // });
  createItemEffect$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ItemsActions.createItem),
      switchMap((item) => {
        return this.data.createItem(item).pipe(
          map((res) => ItemsActions.loadItems()),
          catchError(() => {
            console.log('Fail');
            return of(ItemsActions.createItemError);
          })
        );
      })
    );
  });
  modifyItemEffect$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ItemsActions.modifyItem),
      mergeMap(({ pos, _id, changes }) => {
        return this.data.modifyItem(_id, changes).pipe(
          map((res) => ItemsActions.loadItems()),
          catchError(() => of(ItemsActions.modifyItemError))
        );
      })
    );
  });
  removeItemEffect$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ItemsActions.removeItem),
      switchMap(({ targetId }) =>
        this.data.deleteItem(targetId).pipe(
          map(() => ItemsActions.deleteItemSuccess()),
          catchError(() => of(ItemsActions.deleteItemError))
        )
      )
    );
  });
  deleteItemSuccess$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ItemsActions.deleteItemSuccess),
      map(() => ItemsActions.loadItems())
    );
  });
}
