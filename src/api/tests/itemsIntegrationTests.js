require("should");
const request = require("supertest");
const mongoose = require("mongoose");

process.env.ENV = "Test";

const app = require("../app.js");
const Item = mongoose.model("Item");
const agent = request.agent(app);

describe("Item Crud Test", () => {
  it("should allow an item to be posted and return title and _id", (done) => {
    const itemPost = { title: "Mow the lawn", status: false };
    agent
      .post("/api/items")
      .send(itemPost)
      .expect(200)
      .end((err, result) => {
        result.body.status.should.not.equal(true);
        result.body.should.have.property("_id");
        done();
      });
  });
  afterEach((done) => {
    Item.deleteMany({}).exec();
    done();
  });
  after((done) => {
    mongoose.connection.close();
    app.server.close(done());
  });
});
