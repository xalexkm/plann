const should = require("should");
const sinon = require("sinon");
const itemsController = require("../controllers/itemsController");

describe("Item Controller Tests:", () => {
  describe("Post", () => {
    it("should not allow empty title on post", () => {
      const Item = function (item) {
        this.save = () => {};
      };
      const req = {
        body: {
          status: true,
        },
      };

      const res = {
        status: sinon.spy(),
        send: sinon.spy(),
        json: sinon.spy(),
      };
      const controller = itemsController(Item);
      controller.post(req, res);
      res.status
        .calledWith(400)
        .should.equal(true, `Bad status ${res.status.args[0][0]}`);
      res.send.calledWith("Title is required").should.equal(true);
    });
  });
});
