const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();

const port = process.env.PORT || 3000;

if (process.env.ENV === "Test") {
  console.log("This is a test");
  const db = mongoose.connect(
    "mongodb+srv://xalexkm:Alex160402@plann.1pew6.mongodb.net/plann-test?retryWrites=true&w=majority"
  );
} else {
  console.log("This is for real");
  const db = mongoose.connect(
    "mongodb+srv://xalexkm:Alex160402@plann.1pew6.mongodb.net/plann?retryWrites=true&w=majority"
  );
}

const Item = require("./models/itemModel");
const itemsRouter = require("./routes/itemsRouter")(Item);
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use("/api", itemsRouter);

app.get("/", (req, res) => {
  res.send("Welcome to my API!");
});

app.server = app.listen(port, () => {
  console.log(`Running on port ${port}`);
});

module.exports = app;
