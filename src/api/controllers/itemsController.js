function itemsController(Item) {
  function post(req, res) {
    const item = new Item(req.body);
    if (!req.body.title) {
      res.status(400);
      return res.send("Title is required");
    }
    item.save();
    res.status(201);
    return res.json(item);
  }
  function get(req, res) {
    const query = {};
    if (req.query.title) {
      query.title = req.query.title;
    }
    Item.find(query, (err, items) => {
      if (err) {
        return res.send(err);
      }
      const returnItems = items.map((item) => {
        const newItem = item.toJSON();
        newItem.links = {};
        newItem.links.self = `http://${req.headers.host}/api/items/${item._id}`;
        return newItem;
      });
      return res.json(returnItems);
    });
  }
  return { post, get };
}

module.exports = itemsController;
