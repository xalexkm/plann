const express = require("express");
const itemsController = require("../controllers/itemsController");

function routes(Item) {
  const itemRouter = express.Router();
  const controller = itemsController(Item);
  itemRouter.route("/items").post(controller.post).get(controller.get);
  itemRouter.use("/items/:itemId", (req, res, next) => {
    Item.findById(req.params.itemId, (err, item) => {
      if (err) {
        return res.send(err);
      }
      if (item) {
        req.item = item;
        return next();
      }
      return res.sendStatus(404);
    });
  });
  itemRouter
    .route("/items/:itemId")
    .get((req, res) => {
      const returnItem = req.item.toJSON();
      returnItem.links = {};
      returnItem.links.FilterByThisTitle = `http://${req.headers.host}/api/items/?title=${req.item.title}`;
      res.json(returnItem);
    })
    .put((req, res) => {
      const { item } = req;
      item.title = req.body.title;
      item.status = req.body.status;
      req.item.save((err) => {
        if (err) {
          return res.send(err);
        }
        return res.json(item);
      });
    })
    .patch((req, res) => {
      const { item } = req;
      // eslint-disable-next-line no-underscore-dangle
      if (req.body._id) {
        // eslint-disable-next-line no-underscore-dangle
        delete req.body._id;
      }
      Object.entries(req.body).forEach((change) => {
        const key = change[0];
        const value = change[1];
        item[key] = value;
      });
      req.item.save((err) => {
        if (err) {
          return res.send(err);
        }
        return res.json(item);
      });
    })
    .delete((req, res) => {
      req.item.remove((err) => {
        if (err) {
          return res.send(err);
        }
        return res.sendStatus(204);
      });
    });
  return itemRouter;
}

module.exports = routes;

// const express = require("express");
// function routes(Item) {
//   const itemsRouter = express.Router();
//   itemsRouter
//     .route("/items")
//     .post((req, res) => {
//       const item = new Item(req.body);
//       console.log(item);
//       item.save();
//       return res.status(201).json(item);
//     })
//     .get((req, res) => {
//       const query = {};
//       if (req.query.title) {
//         query.title = req.query.title;
//       }
//       Item.find(query, (err, items) => {
//         if (err) {
//           return res.send(err);
//         }
//         return res.json(items);
//       });
//     });
//   itemsRouter.use("/items/:itemId", (req, res, next) => {
//     Item.findById(req.params.itemId, (err, item) => {
//       if (err) {
//         return res.send(err);
//       }
//       if (item) {
//         req.item = item;
//         return next();
//       }
//       return res.sendStatus(404);
//     });
//   });
//   itemsRouter
//     .route("/items/:itemId")
//     .get((req, res) => res.json(req.item))

//     .put((req, res) => {
//       const { item } = req;
//       item.title = req.body.title;
//       item.status = req.body.status;
//       req.item.save((err) => {
//         if (err) {
//           return res.send(err);
//         }
//         return res.json(item);
//       });
//     })
//     .patch((req, res) => {
//       const { item } = req;
//       if (req.body._id) {
//         delete req.body._id;
//       }
//       Object.entries(req.body).forEach((item) => {
//         const key = item[0];
//         const value = item[1];
//         item[key] = value;
//       });
//       req.item.save((err) => {
//         if (err) {
//           return res.send(err);
//         }
//         return res.json(item);
//       });
//     });
//   return itemsRouter;
// }
// module.exports = routes;
