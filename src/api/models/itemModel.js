const mongoose = require("mongoose");

const { Schema } = mongoose;

const itemModel = new Schema({
  title: { type: String },
  status: { type: Boolean, default: false },
});

module.exports = mongoose.model("Item", itemModel);
